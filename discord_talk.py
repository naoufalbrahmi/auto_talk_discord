#!/usr/bin/env python3

import argparse
import sys
import yaml
import logging
import requests
import re
import time
import random
import os
import subprocess

from pathlib import Path
from subprocess import Popen

logging.getLogger().setLevel(logging.INFO)


# run_process to call extern scripts and launch in terminal used for discord_talk


# TALK Calss to be called when using discord_talk option
class TALK:
    # getting the 5 fields from yaml input
    def get_talk_args(self, input_file):
        # -----------------------------------------------
        # parse input discord_talk.yml
        # -----------------------------------------------
        try:
            with open(input_file, 'r') as f1:
                inRegressYml = yaml.load(f1, Loader=yaml.BaseLoader)
        except OSError as e:
            # There has been an error processing the discord_talk.yml file
            logging.error('[ERROR] No such file or directory: '"'input.yml'")
            sys.exit(1)
        # Create dict to parse talk input from discord_talk.yml
        discord_talk_args_dict = {}
        discord_talk_args_dict = {"users_ids": inRegressYml[0]['users_ids'],
                                  "users_auths": inRegressYml[1]['users_auths'],
                                  "time_sleep": inRegressYml[2]['time_sleep'],
                                  "discord_links": inRegressYml[3]['discord_links'],
                                  "discord_talk_1": inRegressYml[4]['discord_talk_1']}

        # check if a field from discord_talk.yml is empty
        for key in discord_talk_args_dict.keys():
            if discord_talk_args_dict[key] == '' or not discord_talk_args_dict[key]:
                logging.error("while getting the field {} from the input.yml".format(key))
                discord_talk_args_dict[key] == ""
                sys.exit(1)
            else:
                continue
        return discord_talk_args_dict


def send_message(message, authorization, discord_channel):
    payload = {"content": message}
    header = {"authorization": authorization}
    r = requests.post(discord_channel, data=payload, headers=header)


def compose_conversation_from_ids(users_ids, users_auths, time_sleep, discord_links, discord_talk_1):
    regexp_which_user = re.compile(
        r'(?P<user_id>[1-9]|1[012])-(?P<string_part1>.+)@|(?P<tagged_user_id>\d)|(?P<string_part2>.+)')
    regexp_extract = re.compile(
        r'(?P<user_id>[1-9]|1[012])-(?P<string_part1>[^@]+)|@(?P<tagged_user_id>[1-9]|1[012])|(?P<string_part2>.+)')
    len_talk = len(discord_talk_1)
    for i in range(0, len_talk):
        re_match_user = regexp_extract.findall(discord_talk_1[i])
        print(discord_talk_1[i])
        string_part1 = ""
        tagged_id = ""
        string_part2 = ""
        if re_match_user:
            if len(re_match_user) == 1:
                user_id = re_match_user[0][0]
                string_part1 = re_match_user[0][1]
            elif len(re_match_user) == 2:
                user_id = re_match_user[0][0]
                string_part1 = re_match_user[0][1]
                tagged_user_id = re_match_user[1][2]
                tagged_id = "@" + users_ids[int(tagged_user_id) - 1]
            elif len(re_match_user) == 3:
                user_id = re_match_user[0][0]
                string_part1 = re_match_user[0][1]
                tagged_user_id = re_match_user[1][2]
                tagged_id = "<@" + users_ids[int(tagged_user_id) - 1] + ">"
                string_part2 = re_match_user[2][3]
        parsed_line_talk = string_part1 + tagged_id + string_part2
        print("{}  {}  {}".format(parsed_line_talk, users_auths[int(user_id) - 1], discord_links[0]))
        send_message(parsed_line_talk, users_auths[int(user_id) - 1], discord_links[1])
        sleep_var = random.randint(int(time_sleep), int(time_sleep) + 15)
        print(sleep_var)
        time.sleep(sleep_var)


def main():
    # Parse options & provide program documentation
    parser = argparse.ArgumentParser(epilog="Launches the specific discord talk script", description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-y', '--yaml', help="Input input.yml file path", required=False)
    args = parser.parse_args()
    pop = TALK()
    if args.yaml:
        # get talk arguments from the input yaml
        mydict = pop.get_talk_args(args.yaml)
    else:
        # By default took the discord_talk.yml from the same repo, if no --yaml is specified
        mydict = pop.get_talk_args("discord_talk.yml")
    logging.info(" Starting discord talk bot . . . ")
    while True:
        compose_conversation_from_ids(mydict["users_ids"], mydict["users_auths"], mydict["time_sleep"],
                                  mydict["discord_links"], mydict["discord_talk_1"])
    logging.info(" Done discord talk bot for discord_talk_1. . . ")


#
# Main
#
if __name__ == '__main__':
    main()
    sys.exit(0)
